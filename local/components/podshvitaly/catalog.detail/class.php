<?
use Bitrix\Main,
	Bitrix\Main\Loader,
	Bitrix\Iblock\Component\Element,
	Bitrix\Main\Localization\Loc,
	Bitrix\Catalog;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('iblock'))
{
	ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALLED'));
	return;
}

CBitrixComponent::includeComponentClass('bitrix:catalog.element');

class CatalogDetail extends CatalogElementComponent
{
    private $selectColorCode = false;

    public function executeComponent()
    {

        $this->selectColorCode = $this->arParams['OFFERS_SELECT_COLOR']?:false;
        if($this->selectColorCode){
            $colorData = $this->getSkyColor();
            foreach ($colorData as $code=>$color){

                if($color['translate'] == $this->selectColorCode){
                    $this->selectColorCode = $code;
                    break;
                }
            }

        }

        return parent::executeComponent();
    }

    /**
     * Fill additional keys for component cache.
     *
     * @param array &$resultCacheKeys		Cached result keys.
     * @return void
     */
    protected function initAdditionalCacheKeys(&$resultCacheKeys)
    {
        $resultCacheKeys[] = 'skyColor';
    }

    protected function editTemplateJsOffers(&$item, $offerSet)
    {
        parent::editTemplateJsOffers($item, $offerSet);

        foreach ($item['OFFERS'] as $keyOffer => $offer)
        {
            $color = $this->getSkyColor($offer['PROPERTIES']['COLOR_REF']['VALUE']);

            $item['JS_OFFERS'][$keyOffer]['PROPERTY']['TITLE'] = $item['META_TAGS']['TITLE'] . $color['name'];
            $item['JS_OFFERS'][$keyOffer]['PROPERTY']['BROWSER_TITLE'] = $item['META_TAGS']['BROWSER_TITLE'] . $color['name'];;
            $item['JS_OFFERS'][$keyOffer]['PROPERTY']['COLOR'] = $color['name'];
            $item['JS_OFFERS'][$keyOffer]['PROPERTY']['DETAIL_PAGE_URL'] = $item['DETAIL_PAGE_URL'] . $color['translate'] . '/';

            $this->checkModifMeta($item['JS_OFFERS'][$keyOffer]['PROPERTY']);
        }

    }

    protected function initMetaData()
    {


        if ($this->selectColorCode) {
            $this->arResult['META_TAGS']['TITLE'] .=  $this->getSkyColor($this->selectColorCode)['name'];
            $this->arResult['META_TAGS']['BROWSER_TITLE'] .=  $this->getSkyColor($this->selectColorCode)['name'];
        }

//        $this->checkModifMeta($this->arResult['META_TAGS']); реализовано в епилоге каталога

        parent::initMetaData();
    }

    /**
     * @param $offers
     * @param $iblockId
     */
    protected function chooseOffer($offers, $iblockId){

        if ($this->selectColorCode) {
            foreach ($offers as $offer) {
                if (!isset($this->elementLinks[$offer['LINK_ELEMENT_ID']])) continue;

                if ($offer['PROPERTIES']['COLOR_REF']['VALUE'] == $this->selectColorCode) {
                    $this->elementLinks[$offer['LINK_ELEMENT_ID']]['OFFER_ID_SELECTED'] = $offer['ID'];

                    return;
                }
            }
        }

        parent::chooseOffer($offers, $iblockId);
    }

    protected function checkModifMeta(&$object){
        $data = [];
        $data['h1'] = Main\Config\Option::get('podshvitaly.test', 'h1');
        $data['title'] = Main\Config\Option::get('podshvitaly.test', 'title');

        if($data['h1']){
            $object['TITLE'] = str_replace('#H1#', $object['TITLE'], $data['h1']);
        }
        if($data['title']){
            $object['BROWSER_TITLE'] = str_replace('#TITLE#', $object['BROWSER_TITLE'], $data['title']);
        }
    }

    protected function getSkyColor($code = false){
        if(empty($this->arResult['skyColor'])){

            $hlColorID = 2; // todo: вывести в параметры
            Loader::includeModule("highloadblock");
            $hl = Bitrix\Highloadblock\HighloadBlockTable::getById($hlColorID)->fetch();
            $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hl)->getDataClass();
            $iterator = $entity::getList();

            while ($item = $iterator->fetch()) {

                $this->arResult['skyColor'][$item['UF_XML_ID']]['name'] = $item['UF_NAME'];
                $this->arResult['skyColor'][$item['UF_XML_ID']]['translate'] = $this->translateName($item['UF_NAME']);
            }
        }
        if (!$code) {
            return $this->arResult['skyColor'];
        } else {
            return $this->arResult['skyColor'][$code];
        }
    }

    protected function translateName($name){
        return Cutil::translit($name, "ru", array("replace_space" => "-", "replace_other" => "-"));
    }
}