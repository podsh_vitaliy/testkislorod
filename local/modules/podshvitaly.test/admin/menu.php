<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

//use Bitrix\Main\Localization\Loc;
//
//Loc::loadMessages(__FILE__);

$aMenu = array(
    array(
        'parent_menu' => 'global_menu_content',
		'section' => "innovative",
        'sort' => 400,
		"icon" => "update_menu_icon_partner",
		"page_icon" => "update_menu_icon_partner",
        'text' => "Модуль для тестового задания",
        'title' => "Модуль для тестового задания",
        'url' => '/bitrix/admin/settings.php?lang=ru&mid=podshvitaly.test&mid_menu=1',
        'items_id' => 'podshvitaly.test',
        'items' => array(),
    ),
);

return $aMenu;
