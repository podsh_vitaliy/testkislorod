<?php
function checkModifMetaCatalog(){
    global $APPLICATION;

    $h1 = $APPLICATION->GetTitle();
    $title = $APPLICATION->GetPageProperty('TITLE');

    $data = [];
    $data['h1'] = \Bitrix\Main\Config\Option::get('podshvitaly.test', 'h1');
    $data['title'] = \Bitrix\Main\Config\Option::get('podshvitaly.test', 'title');

    if($data['h1']){
        $h1 = str_replace('#H1#', $h1, $data['h1']);
    }
    if($data['title']){
        $title = str_replace('#TITLE#', $title, $data['title']);
    }

    $APPLICATION->SetTitle($h1);
    $APPLICATION->SetPageProperty('TITLE', $title);
}