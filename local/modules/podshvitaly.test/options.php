<?
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();
use    Bitrix\Main,
        Bitrix\Iblock,
        Bitrix\Catalog;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;

$moduleID = 'podshvitaly.test';

Loader::includeModule($moduleID);
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
$data = [];
if($request->get('save')){
    $data = $request->get('data');

    Option::set($moduleID, 'h1', $data['h1']);
    Option::set($moduleID, 'title', $data['title']);
}else{
    $data['h1'] = Option::get($moduleID, 'h1');
    $data['title'] = Option::get($moduleID, 'title');
}



$aTabs = array(
        array('DIV' => 'tab1', 'TAB' => "Главная", 'ICON' => '', 'TITLE' => 'Настройка'),
);

$tabControl = new CAdminTabControl('tabControl', $aTabs);

$tabControl->Begin();


?>
<form action="<?= sprintf('%s?mid=%s&lang=%s', $request->getRequestedPage(), urlencode($mid), LANGUAGE_ID) ?>"
      method="post">
    <?$tabControl->BeginNextTab();?>
    <div class="">
        <label for="h1">h1</label>
        <input id="h1" type="text" name="data[h1]" value="<?= $data['h1']?>">
    </div>
    <div class="">
        <label for="title">title</label>
        <input id="title" type="text" name="data[title]" value="<?= $data['title']?>">
    </div>



    <? $tabControl->Buttons(array(
            "back_url" => $_REQUEST["back_url"],
            "btnApply" => false,
    )); ?>

</form>
<?

$tabControl->End();
?>
